/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

import javax.persistence.Entity;
import javax.persistence.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author jsnei
 */
@Entity
@Table(name="administrador")


public class Administrador {
    
    @Autowired
    transient JdbcTemplate jdbcTemplate;
    
    
    private int idAdministrador;
    
    private String nombreAdministrador;
    
    private String clave;
            
}

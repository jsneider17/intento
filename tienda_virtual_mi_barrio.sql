create database tienda_virtual_mi_barrio;
use  tienda_virtual_mi_barrio;	


CREATE TABLE administrador (
  id_administrador INT NOT NULL,
  nombre_administrador varchar(45) NOT NULL,
 clave varchar(20) NOT NULL,
 
  constraint administrador_pk PRIMARY KEY (id_administrador));
  
  
   CREATE TABLE sucursal (
  id_sucursal int NOT NULL AUTO_INCREMENT,
   ciudad varchar(50) NOT NULL,
   direccion varchar(50) NOT NULL,
    id_venta_ingreso varchar(50) NOT NULL,
  id_compra_egreso varchar(50) NOT NULL,
  id_administrador int NOT NULL,
  constraint sucursal_pk PRIMARY KEY (id_sucursal),
   CONSTRAINT sucursal_id_administrador_fk FOREIGN KEY (id_administrador) REFERENCES administrador (id_administrador));
  
  
  CREATE TABLE bodega (
  id_bodega int NOT NULL AUTO_INCREMENT,
  nombre varchar(50) NOT NULL,
  cantidad varchar(50) NOT NULL,
  id_sucursal int not null, 
  constraint bodega_pk PRIMARY KEY (id_bodega),
  CONSTRAINT bodega_id_sucursal_fk FOREIGN KEY (id_sucursal) REFERENCES sucursal (id_sucursal));
  
    

 
 CREATE TABLE ingreso (
   id_venta varchar(15) NOT NULL,
   id_producto varchar(50) NOT NULL,
   cantidad varchar(50) NOT NULL,
   ingreso_total varchar(50) NOT NULL,
   id_sucursal int not null,
  constraint ingreso_pk PRIMARY KEY (id_venta),
  constraint ingreso_id_sucursal_fk FOREIGN KEY (id_sucursal) REFERENCES sucursal (id_sucursal));
  
  
  
    CREATE TABLE egreso (
   id_compra varchar(15) NOT NULL,
   id_producto varchar(50) NOT NULL,
   cantidad varchar(50) NOT NULL,
   id_proveedor varchar(50) NOT NULL,
   nomina varchar(50) NOT NULL,
   servicios varchar(50) NOT NULL,
	id_sucursal int not null,
  constraint egreso_pk PRIMARY KEY (id_compra),
  constraint egreso_id_sucursal_fk FOREIGN KEY (id_sucursal) REFERENCES sucursal (id_sucursal));